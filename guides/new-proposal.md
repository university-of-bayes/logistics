# Submitting a New Proposal

First, check if asking on the server ([\#caretaking-uofbayes](https://discord.com/channels/713475280219537500/713475280668459080)) is enough.  No need for bureaucratic overhead where it isn't needed!

If you *do* think that this is the right place, then there's three possible levels:

 -  Make a single Markdown file in [`/proposals`](../proposals).  (If you don't
    know your way around Git, you can use the GitLab web UI for that: There's a
    '+' dropdown menu at the end of the path shown near the top of the page.
    Select 'new file', and you get a text field to type or paste your idea.)
    Make sure to:
     +  Give it a good file name, and don't forget the `.md` extension.  Try to
        _summarize the solution, not the problem._  (If it's not clear yet what
        possible solutions are, then discuss on the server first!)  Keep the
        name all lower case and replace spaces in the name with dashes.  So a
        good name would be `fewer-voice-channels.md`, whereas
        `too-many-channels.md`, `Let's get rid of some voice channels.md`,
        `get-rid-of-voice-channels` are all less good.
     +  Add a commit message that's better than just "Add new file", something like
        `new proposal: fewer voice channels` is enough.

 -  Alternatively, make a whole directory in [`/proposals`](../proposals).  Put
    the main text of the proposal into a `README.md` inside the directory (that
    file is shown when you open that directory in the web UI) and name the
    directory like in the single file case above, e. g. `fewer-voice-channels`.
    Any other files in that folder, you probably know best what works for you.

 -  Or make a full branch (probably only needed in case you want to reorganize
    this repository.)  Name the branch by the dashed-lowercase convention, then
    add a Markdown file with the same name in [`/proposals`](../proposals).
    That's the place to put any extra info, and it's also useful to make people
    aware that the branch exists.

For the main proposal file, here's a template that you can paste and fill in:

```md
# ((Your Proposal Title Here))

## Overview

### What?

((What's the plan, roughly? Keep it short!))

### Why?

((Does this try to solve a problem? Is it just a cool idea?))

## ((Details))

((structure & content as you think is best))
```

