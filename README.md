# University of Bayes – Logistics

This is the place to find documentation on the current state, planned changes,
and general processes on the University of Bayes Discord server.

The main categories of information are:

## [Guides](guides) - How do I…?

Look here for information on how things work.  Want to organize an event, run a
course, propose changes?  Find checklists and names of people to ask for support!

## [Maps](maps) - What's the current state of…?

Look here for a reasonably accurate snapshot of the current situation on the
server.  Channel lists, role purposes, other static information is all here.

## [Proposals](proposals) - What if we…?

Look here to see how things might change.  See proposed changes, give feedback,
or make your own suggestions!

