
Note: this will be automatically transformed to make the \#server-map and other things. So please stick to the format!

Lines starting with a `#` will end up in the server map, (almost) everything else is ignored for that purpose. `##` is a category, `###` a channel in that category, and `####` a thread in that channel. Name first, exactly as it appears (it'll get turned into a link), then space two hyphen-minuses space ` -- `, then a short(!) description that should appear in the map.

Except: #FIXME for now, we should include info on what roles unlock a channel, which probably is too much for one line; so, _for now_, we should probably have two lines or something like that. Alternate format therefore: a line _immediately below_ a heading, with no empty line in between, will also be transcluded somehow.

Also #FIXME: explicit links for voice etc

Anything that's not a heading is intended to be the channel description (or pinned first message for threads, because they don't have a description.) There's space for 1024 characters, it can definitely be used to give guiding advice! (_Do_ try to keep the first line short enough to show in full.)

Anything above the line just below will disappear, anything below it and above the first heading will be copied over into the \#server-map, as introductory text.

((end of part that will disappear))

-----

This is an overview of all public channels on the server. Because there are _a lot_ of channels, most are hidden by default. To access the ones you're interested in, get either {I View All Channels :IveSeenEverything:} (i.e. the "View All Channels" role in block 1), or one specifically listed for the channel you want.

*Almost* all of these roles can be self-assigned in #role-acquisition, some old ones no longer have a reaction trigger and need to be elf-assigned – ask any "Associate!" or Moderator to help you. (Or just get the "View All Channels" role.)

If you're not sure what you want to see, get the "View all Channels" role and just observe where stuff happens for a week or two. Then, you can either selectively mute channels that you don't care about, or try to find a role combination that hides most of the stuff that you don't want.

## Information -- 

### announcements-and-events -- general announcements

Announcements, upcoming events and other need-to-know information goes here!

### welcome -- server overview

Welcome to the University of Bayes! A place for education, collaboration and conversation.

### role-acquisition -- roles for making channels visible & more

Get your roles here! Fresh tasty roles! Get them by the dozen!

### introductions -- describe yourself!

Optional! Whether you've been here for months or minutes, you're welcome to have an entry here and update it. Modify the template however you want.

Please refrain from discussion in this channel, to make it easy to see all the introductions!

### recommended-servers -- other cool places

Share your own personal servers or other recommended servers here!

### server-map -- you are here

under construction

## General -- 

### the-lounge -- 

A general place to hang out.  
Puns permitted and encouraged.

### starboard -- 

Messages with 3+ :star: reacts show up here. Share the highlights/valuable bits from other channels!

### general-voice-text-01 -- 

Join for some chill convo, probably.

### general-voice-text-02 -- 

### watch-party-text -- 

### <#751270609794760715> -- 

### <#751270701385777212> -- 

### <#867405507811737620> -- 

## Arts, Literacy & Philosophy -- 

### the-multimedia-library -- 
{I Arts & Literacy :bird:}

The Great Library of Alexandria was one of the largest in the ancient world. Let's make ours better :D

Share what you're reading or recommendations for fun or edification! Or both!

We also welcome what you are watching, any videos, music, podcasts, or shows are welcome to be shared here.

We want ALL the resources!

### creative-endeavours -- things you made yourself
{I Arts & Literacy :bird:}

This is a place to share or collaborate on creative endeavours – things like artwork, music, poetry, needlework, making clothing, and yes, memes in general!

### language-practice-n-games -- learning languages
{I Arts & Literacy :bird:}

This channel if for practicing languages. If you're new (or old) to English, and want some feedback, post here! If you want to write in or read a different language, this is the place for you too!

### meditation -- 
{I Philosophical & Spiritual :angel:}

This channel for encouraging and discussing the use of meditation :galaxybrain:

### music-theory -- 
{I Arts & Literacy :bird:} {V Music Theory :musical_note:}

A space to learn what makes music sound good. Check pins for resources!

### philosophical-and-spiritual -- 
{I Philosophical & Spiritual :angel:}

This channel is for discussion of your own philosophy, the works of philosophers, general moral reasoning, and things of spiritual interest.

### shakespeare -- 
{V Shakespeare's Plays :crossed_swords:}

### <#713887210386227201> -- 
{I Arts & Literacy :bird:} {II any of the language roles :england: :flag_fr: :flag_ea: :flag_jp: :flag_de: :flag_ru: :flag_pt: :flag_cn: :flag_eh: :pinching_hand: :hand_splayed:}

### <#750785411407675442> -- 
{I Philosophical & Spiritual :angel:} {III Ping: Meditation :enlightened:}

## Projects -- 

### lectures -- 

If you have a youtube video or channel, or other online learning tool or resource that you really like, share it here!

### vr-discussion -- 
{I General Learning :tools:}

### plans-and-projects -- 

This chat is for sharing plans for feedback, and/or to find out if others are interested in shared projects. These could be of personal or global significance, or anything in-between… or beyond!

(Thanks IonSprite for the brilliant channel name… Gaussian Plots and Plans. RIP XorAnder22's whimsy :P)

### triumph-of-individual-style -- 
{I General Learning :tools:}

The book Triumph of Individual Style is about body-acceptance, learning about your own body's beauty and how to achieve the look you want using clothes and accessories.

### agentyduck-book-club -- 
{I General Learning :tools:} {n/a AgentyDuck Book Club —}

We read agentyduck.blogspot.com together, then meet once a week to discuss and try out the tools.

### fixing-broken-educational-systems -- 
{V Fixing BES :school:}

This channel is for discussing, sharing resources, and working towards fixing broken educational systems.

### trivia -- 
{I General Learning :tools:} {V Trivia :bulb}

A quiz/trivia channel that can be there for fun, and also be used for things like trivia game nights that Leo will be hosting

### the-signal-and-the-noise -- 
{I General Learning :tools:}

This place is for plans and discussions of the future and what to do in the present to prepare for it or shape it.

### skills-n-knowledge-swap -- 
{I General Learning :tools:}

Share cool tools, tips, trivia and more from any source. No knowledge is insignificant!

### supermemo-rationality-improvement -- 
{V SuperMemo :man_superhero:}

For discussion of SuperMemo, incremental reading, rationality, time management and self improvement. Aka. Becoming Powerful

### topics-of-the-month -- 

### podcast-uofbayes -- 
{n/a Recording —}

The University of Bayes is starting a podcast!

### co-working-voice-text -- 
{I General Learning :tools:} {III Study Buddy :books:}

This space is for co-working with others so it doesn't tie up other chats.

If you need other space, UofBayes "officially" recommends using some great open source software with no download needed, and point-to-point (end-to-end in beta) encryption: https://meet.jit.si/

### darwin-game -- 

Programming contest for Ripple's twist on the Darwin Game programming contest

### finance -- 
{V Finance :money_with_wings:}

### <#832794465621114881> -- 
{I General Learning :tools:} {III Study Buddy :books:}

### <#714138306241101835> -- 

### <#742066088497381527> -- 
{n/a Recording —}

## STEM -- 

### calc-class -- 
{I STEM :woman_scientist:} {V Calculus :rocket:}

### computing-n-data-analysis -- 
{I STEM :woman_scientist:}

### folding-at-home -- 
{I STEM :woman_scientist:}

Help scientists by donating your computing power, visit https://foldingathome.org/.

### laplacian-labs -- 
{I STEM :woman_scientist:} {V Laplacian Labs :desktop:}

The statistics and mathematical modelling arm of the University of Bayes. Post requests for proposals here.

### causal-inference -- 
{I STEM :woman_scientist:} {V Causal Inference :arrow_up_down:}

### statistical-rethinking -- 
{I STEM :woman_scientist:} {n/a Stat Rethinking —} {V Stat Rethinking Group 2 :chart_with_upwards_trend:} {V Stat Rethinking Group 3 :brain:}

This channel is for discussion and organization of groups going over Dr. Richard McElreath's "Statistical Rethinking" course, which covers an intro to Bayesian analyses, as well as the creation of statistical models.

### intro-to-stats -- 
{I STEM :woman_scientist:} {V Intro to Stats :telescope:}

### maxwells-equations -- 
{I STEM :woman_scientist:} {V Maxwell's Equations :magnet:}

### picturing-quantum-processes -- 
{I STEM :woman_scientist:} {V Picturing Quantum Processes :flying_saucer:}

### jaynes-probability -- 
{I STEM :woman_scientist:} {V Jaynes Probability Theory (still shows as "Statistics 110") :roller_coaster:}

Channel for discussion of E.T. Jaynes' book: Probability Theory: The Logic of Science.  
https://bayes.wustl.edu/etj/prob/book.pdf

### regression -- 
{I STEM :woman_scientist:} {V Regression :tap:}

This channel is for discussion of the book Regression and Other Stories

### computational-biology -- 

### STEM Voice -- 

## Flourishing -- 

### calls-for-help -- 

This is our channel for immediate calls for help. If you need help with ANYTHING, post it here.

### job-board -- 

If you're looking for a job, want to hire, or come across a good opportunity, this is the place to share it!

### mental-flourishing -- 

This is a place where you can share your own problems and approaches, as well as mental hacks and thoughts on positive wellbeing. 

Our default tendency is to offer advice or thoughts, though in a respectful way – if you purely want a supportive environment to know that you are not alone, please post in <#714271602745016351>

### real-talk-no-advice-please -- 

[SERIOUS] \#real-talk is a channel where people can post to get support. In this channel, someone who starts a conversation gets to be the center of the conversation and all responses should be focused on them – e.g. if you are listening, don't question whether they really have a problem, don't compare their problem to others, don't talk about your own experience at length, don't give advice or problem-solve. 

Don't try to cheer people up in here; meet them where they're at and witness their experience. Just being heard and believed can be really beneficial. 
(Request advice in <#716274295751311360>, any mental health discussion can also go in <#713479601061494864>.)

### daily-wins -- 
{I Flourisher :rainbow:}

Post your win of the day! (Multiple posts per day allowed.)

### food-and-sustainability -- 
{I Flourisher :rainbow:} {V Food & Sustainability :mushroom:}

Working group for building a delicious and healthy vegetarian cookbook. Also discussion of homesteading, permaculture, wild greens foraging, and related topics.

### health-and-fitness -- 
{I Flourisher :rainbow:} {V Health and Fitness :salad:}

A sound mind in a sound body!  
– Thales of Miletus

Share advice on physical fitness and health here! Keep in mind "we are not medical doctors"… unless some of us are!

### change-my-view -- 
{I Willing to change views :thonk:}

Voice chat strongly encouraged! This is a place for people who are seeking to change views. It is a safe space for ALL ideas. Conversations are still to be based off of respect. If you are sharing an idea, provide your thoughts/reasons for your beliefs, to the extent you have them. In other words, aim to provide your thoughts rather than just your conclusions.

### friend-finding -- 
{I Flourisher :rainbow:}

This channel is for finding friends without the mystery. Post here if you are looking for new friends.

### diversity-appreciation -- 
{I Flourisher :rainbow:}

Actions, personal experiences, and a safespace regarding racism, sexism, lgbtq, and neurodivergence -appreciation for all ways of being human.

### pacts-and-promises -- 
{I Flourisher :rainbow:}

This place is for producing pacts and promises to promote productivity.

### politics-policy -- 
{I Flourisher :rainbow:}

Voice chat strongly encouraged! A channel for political discourse.

For long-form thoughtful discussion emphasizing rigour.

### relationship-questions-and-advice -- 
{I Flourisher :rainbow:}

### shouting-only -- 
{I Flourisher :rainbow:}

SHOUTING ONLY! EXPERIMENTAL CHANNEL! USE AT OWN RISK!

### self-improvement -- 
{I Flourisher :rainbow:}

### sexuality-gender-q-and-a -- 
{I Flourisher :rainbow:}

This channel is for discussing ideas and personal issues/thoughts/questions regarding sexuality and gender. No topic is taboo here (seriously! Even very challenging/taboo topics in other places in society), so you can likely consider this channel "Not Safe For Work", though we do consider it safe for people of all ages, provided they are mature.

Do not share any pornographic content or links to such content here. That includes images, videos and written erotica.

### <#719477937589715075> -- 
{I Flourisher :rainbow:}

### <#714279376115728415> -- 

## Server Meta --  keeping this place running

### caretaking-uofbayes -- administrative suggestions & discussions

General discussions for the administration here Administration is not a position that I want to receive much in the way of accolades, though it is valuable work. You can think of this as custodial responsibilities.

### vote -- 

This channel is for voting for changes that you'd like to see in the server. If you have any thoughts, please discuss them in <#713475280668459080>.

### probot-playground -- 

To open a ticket, type in -ticket open

### feedback-lectures-and-others -- 
{I Meta :snowman2:} {III Give me feedback about myself :sparkling_heart:}

Use of Voltaire encouraged! Give feedback to people (especially peeps with the feedback role) about themselves, events and workshops they ran, or anything

 @Voltaire (me) a DM using this format send_server "University of Bayes" feedback {message}

### <#764515654367182889> -- 

## Tickets (Opportunities) -- 
{I Meta :snowman2:}

(omitted)

## Archive -- where the skeletons hang out
{I Ancient Librarian :books:}

(omitted)
